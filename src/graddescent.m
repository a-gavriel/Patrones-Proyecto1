%graddescent

% usage graddescent(Ndatos=5000, Nclases=3 , shapeName= 'radial', neuronascapa1=8, 
%                  batchSize=10, lambda=1e-2, total_epoch=1000, min_error=1.8e-9 )
% 
% Esta funcion realiza un entrenamiento de la red neuronal descrita por los parámetros de entrada
% 
% Inputs:
%   Ndatos: Total de datos a generar para entrenar la red
%   Nclases: Número de clases de salida
%   shapeName: distribution of the samples in the input space:
%          'radial' rings of clases,
%          'pie' angular pieces for each class
%          'vertical' vertical bands
%          'horizontal' horizontal bands
%   neuronascapa1: Número de neuronas en la capa oculta
%   batchSize: Tamaño de cada minilote a evaluar
%   lambda: Tamaño de paso al modificar los pesos
%   total_epoch: Número máximo de épocas a iterar
%   min_error: Error buscado para la convergencia
%   show_error: Boleano para mostrar o no las gráficas del error
%
% Outputs:
%   Figura 1 Datos de entrenamiento
%   Figura 2 Gráfica de error con respecto a la época
%   W1 W2: Matrices con los pesos obtenidos, se guardan en la carpeta 'weigths'
%   Description: Descripción de la configuración y resultado del entrenamiento, se guarda en la carpeta 'weigths'
function graddescent(Ndatos=5000, Nclases=4 , shapeName= 'radial', neuronascapa1=8, 
              batchSize=10, lambda=1, total_epoch=1, min_error=3.1e-12, show_error=false)

neuronascapa2=Nclases;

lddata = input("New training? (0-1): ");
if (!lddata)
  [X,Y,W1,W2]=loaddata(Nclases,shapeName,neuronascapa1);
  if (!X)
    return;
  endif
else
  [X,Y]=create_data(Ndatos,Nclases,shapeName);
  hasdistance = input("Use distance? (0-1): ");
  if hasdistance
    X = [X (sum(X.*X,2))];
  endif
  W1=randn(neuronascapa1,columns(X)+1);
  W2=randn(neuronascapa2,neuronascapa1+1);
endif

%se grafican los datos creados
figure(1,'name','Datos de entrenamiento');
hold off;
plot_data(X,Y) 

%Acumulador del error total para graficar
j_total=[];
epoch_counter=1;

%Cantidad de lotes a iterar
num_batches=ceil(rows(X)/batchSize);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% declaración de capas

global l1a=fullyconnectedbias();
global l1b=sigmoide();
global l2a=fullyconnectedbias();
global l2b=sigmoide();

%Gráfico del error para la cantidad de épocas dadas
if (show_error)
  disp("Graficando el error");
  fflush(stdout);
  plot_error(W1,W2,X,Y,Nclases,25);
endif

% error por época
best_epoch = [1,1]; %El menor error y en cual corrida
best_dif_epoch = [1,1]; %La mejor diferencia de la corrida y en cual época
j_epoch = []; %Error total de una época
dif_epoch =0; %Diferencia del error entre época actual y anterior
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
while(1)  %cantidad de epocas
  
  j_epoch = []; %Reinicio del error total de una época
  
  idx=randperm(rows(X)); %Vector de índices desordenados
  
  %Cálculo por cada mini lotes (Excepto el último)
  for i=1:num_batches-1  %% itera por cada uno de los batches (excepto el último)

    j=idx(batchSize*i-batchSize+1:batchSize*i); %Vector con índices aleatorios

    X_batch=X(j,:); %Obtiene el batch de los índices aleatorios
    Y_batch=Y(j,:); %Valores de Y para el batch elegido

    j_batch=target(W1,W2,X_batch,Y_batch); %Error
    j_epoch = [j_epoch;j_batch]; %Acumulación del error total

    [gW1,gW2]=gradtarget(W1,W2,X_batch,Y_batch); %Gradiente del error

    W1-=lambda*gW1 ;%Actualización de los pesos W1
    W2-=lambda*gW2 ;%Actualización de los pesos W2
    
  endfor #Terminan todos los mini-batches

  %%%%%%%%%%%%%%%%cálculo del último minilote
  j=idx(batchSize*(num_batches-1)+1:rows(X)); %Vector con índices aleatorios

  X_batch=X(j,:); %Inicio de batch de X
  Y_batch=Y(j,:); %Valores de Y para el batch

  j_batch=target(W1,W2,X_batch,Y_batch); %Error
  j_epoch = [j_epoch ; j_batch]; %Acumulación del error total

  [gW1,gW2]=gradtarget(W1,W2,X_batch,Y_batch); %Gradiente del error

  W1-=lambda*gW1 ;%Actualización de los pesos W1
  W2-=lambda*gW2 ;%Actualización de los pesos W2
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  j_epoch=sum(j_epoch); %Suma el error total de la época
  j_epoch=j_epoch/Ndatos; %Normaliza el error de época
  j_total = [j_total;j_epoch]; %Acumula el error entre épocas

  %Actualización de gráficas
  figure(2,'name','Gráfica de error');
  plot(0:rows(j_total)-1,j_total, "linewidth", 3, "color", "m");
  xlabel("epoch");
  ylabel("error");
  refresh();

  %Guarda el valor de la mejor época del error
  if (j_epoch<best_epoch)
    best_epoch = [j_epoch,epoch_counter];
  endif

  %Entra si hay más de una época guardada
  if (epoch_counter>1)
    
    dif_epoch = abs(j_epoch-j_total(end-1));

    %Guarda el valor de la mejor diferencia de épocas
    if (dif_epoch<best_dif_epoch(1))
      best_dif_epoch = [dif_epoch,epoch_counter];
    endif

    %Criterio de finalización
    if ((dif_epoch<min_error))||(epoch_counter>total_epoch)
      %Estadísticas
      printf("Convergencia en época: %i\n", epoch_counter);
      printf("Error: %e\n", j_epoch);
      printf("Diferencia de Error: %e\n", dif_epoch);
      printf("Mejor Error: %e en época %i\n", best_epoch(1),best_epoch(2));
      printf("Mejor Diferencia de Error en época %i: %e\n", best_dif_epoch(1),best_dif_epoch(2));
      break;
    endif

  endif

  epoch_counter+=1; %epoch_counter de epocas

endwhile #Terminan todas las épocas
toc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Guardado de las matrices calculadas a archivos csv
saveweights("gd",X,Y,W1,W2,shapeName,Nclases,neuronascapa1,
    Ndatos,batchSize,total_epoch,min_error,j_epoch,
    lambda,epoch_counter,dif_epoch,best_epoch,best_dif_epoch);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Evaluación de resultados
%Ndatos=1000;
%Se crea un set de datos nuevo con la misma distribución
%[Xnew,Ynew]=create_data(Ndatos,Nclases,shapeName);

%Descripción de una matriz de confusión

%prediccón de los datos de verificación

%desplegar matriz de confusión

%métricas de evaluación de clasificación se pueden derivar de la matriz
%de confusión, en particular la sensitividad, la precisión, el valor F1, 
%y calcúlelas para sus datos.
endfunction