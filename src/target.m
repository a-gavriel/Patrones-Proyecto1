function y=target(W1,W2,X,Y)
    
  % usage target(W1,W2,X,Y)
  % 
  % This function evaluates the sum of squares error for the
  % training set X,Y given the weight matrices W1 and W2 for 
  % a two-layered artificial neural network.
  % 
  % W1: weights matrix between input and hidden layer
  % W2: weights matrix between the hidden and the output layer
  % X:  training set holding on the rows the input data, plus a final column 
  %     equal to 1
  % Y:  labels of the training set
  
  Y_pred = [];
  for (i = 1:rows(X)) %Cantidad de datos en el vector. Debería ser un mini-batch
    exp = predict(W1,W2,X(i,:)); %Valor de la predicción (experimental)
    
    Dif = Y(i,:)'-exp; %Primer resultado
    Y_pred = [Dif , Y_pred]; %Matriz de diferencias acumuladas
  endfor
  
  J = Y - Y_pred';
  y=sum(sum(J.^2,2))*0.5;
  
endfunction;
