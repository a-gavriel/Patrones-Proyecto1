% 

function [X,Y,W1,W2] = loaddata( Nclases=3, shapeName= 'radial', neuronascapa1=8 )
	shapeL = shapeName(1);
	W1name = ["../resources/W1"  shapeL   int2str(Nclases)  ".csv" ];
	W2name = ["../resources/W2"  shapeL   int2str(Nclases)  ".csv" ];
	Xname = ["../sets/X"  shapeL   int2str(Nclases)  ".csv" ];
	Yname = ["../sets/Y"  shapeL   int2str(Nclases)  ".csv" ];

	try
    W1=csvread (W1name);
    W2=csvread (W2name);
    X=csvread (Xname);
    Y=csvread (Yname);
  catch
    disp("Los archivos de entrenamiento no se encontraron!");
    X=0;Y=0;W1=0;W2=0;
    
  end_try_catch

  if (rows(W1)!=neuronascapa1)
    disp(["Los tamaños no coinciden! neuronascapa1 debe ser: " int2str(rows(W1))])
    X=0;Y=0;W1=0;W2=0;
  elseif (rows(W2)!=Nclases)
    disp(["Los tamaños no coinciden! Nclases debe ser: " int2str(rows(W2))])
    X=0;Y=0;W1=0;W2=0;
  elseif (columns(X)+1 != columns(W1))
    disp(["Los tamaños de matrices no coinciden!"])
    X=0;Y=0;W1=0;W2=0;
  elseif ((neuronascapa1+1) != columns(W2))
    disp(["Los tamaños de matrices no coinciden!"])
    X=0;Y=0;W1=0;W2=0;
  endif

endfunction