function saveweights(gradtype="gd",X,Y,W1,W2,shapeName,Nclases,neuronascapa1,
          Ndatos,batchSize,iter,min_error,y,lambda,epoch_counter,
          dif_epoch,best_epoch,best_dif_epoch)
  %Guardado de las matrices calculadas a archivos csv
  try %Verifica si hay carpeta resources creada
    mkdir("../resources");
  catch
    printf("Carpeta 'resources' ya creada");
  end_try_catch
  try %Verifica si hay carpeta sets creada
    mkdir("../sets");
  catch
    printf("Carpeta 'sets' ya creada");
  end_try_catch

  shapeL = shapeName(1); %inicial del nombre de la figura (v h p r)
  W1name = ["../resources/W1"  shapeL   int2str(Nclases)  ".csv" ];
  W2name = ["../resources/W2"  shapeL   int2str(Nclases)  ".csv" ];
  Dname  = ["../resources/description" shapeL int2str(Nclases) ".txt" ];
  Xname = ["../sets/X"  shapeL   int2str(Nclases)  ".csv" ];
  Yname = ["../sets/Y"  shapeL   int2str(Nclases)  ".csv" ];
  csvwrite (W1name, W1);
  csvwrite (W2name, W2);
  csvwrite (Xname, X);
  csvwrite (Yname, Y);
  desc_file = fopen(Dname,"w+");

  %Archivo descriptor de la red y sus resultados
  if strcmp(gradtype, "cg_min")
    fdisp(desc_file,["Entrenado usando cg_min"]);
    fdisp(desc_file,["Neuronas " int2str(neuronascapa1)]);
    fdisp(desc_file,["Clases " int2str(Nclases)]);
    fdisp(desc_file,["Iteraciones " int2str(iter)]);
    fdisp(desc_file,["Datos " int2str(Ndatos)]);
    fdisp(desc_file,shapeName);
    fdisp(desc_file,["batchSize " int2str(batchSize)]);
    fdisp(desc_file,["Buscando una diferencia de error menor a " num2str(min_error)]);
    fdisp(desc_file,["Error: " num2str(y)]);
  elseif strcmp(gradtype , "gd")
    fdisp(desc_file,["Entrenado usando graddescent"]);
    fdisp(desc_file,["Neuronas " int2str(neuronascapa1)]);
    fdisp(desc_file,["Clases " int2str(Nclases)]);
    fdisp(desc_file,["Épocas " int2str(iter)]);%total_epoch
    fdisp(desc_file,["Datos " int2str(Ndatos)]);
    fdisp(desc_file,shapeName);
    fdisp(desc_file,["Batchsize " int2str(batchSize)]);
    fdisp(desc_file,["Lambda " int2str(lambda)]);
    fdisp(desc_file,["Buscando una diferencia de error menor a " num2str(min_error)]);%
    fdisp(desc_file,["Convergencia en época: " num2str(epoch_counter)]);
    fdisp(desc_file,["Error: " num2str(y)]);%j_epoch
    fdisp(desc_file,["Diferencia de Error: " num2str(dif_epoch)]);
    fdisp(desc_file,["Mejor Error: " num2str(best_epoch(1)) " en época: " num2str(best_epoch(2))]);
    fdisp(desc_file,["Mejor Diferencia de Error en época " num2str(best_dif_epoch(1)) " : " num2str(best_dif_epoch(2))]);
  endif
  fclose(desc_file);
endfunction