function plot_error(W1,W2,X,Y,Nclases,max_epoch=100)

  test_batch=[100,50,25,10,1];
  test_lambda=[1e2,1e1,1,1e-1,1e-2];

  %Visualización de la evolucion del error
  figure(10,'name','Gráfica error respecto a los lotes')
  hold on;  
  title ('Con diferentes batchSize');
  ylabel ('Error (J)');
  xlabel ('Cantidad de iteraciones');

  plot1 = [];
  plot1 = neuronalerror(test_batch(1),1,X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('g',';','batchSize=',int2str(test_batch(1)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  plot1 = [];
  plot1 = neuronalerror(test_batch(2),1,X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('m',';','batchSize=',int2str(test_batch(2)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  plot1 = [];
  plot1 = neuronalerror(test_batch(3),1,X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('r',';','batchSize=',int2str(test_batch(3)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  plot1 = [];
  plot1 = neuronalerror(test_batch(4),1,X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('c',';','batchSize=',int2str(test_batch(4)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  plot1 = [];
  plot1 = neuronalerror(test_batch(5),1,X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('y',';','batchSize=',int2str(test_batch(5)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  figure(11,'name','Gráfica error respecto a los lotes')
  hold on;  
  title ('Con diferentes lambdas');
  ylabel ('Error (J)');
  xlabel ('Cantidad de iteraciones');

  plot1 = [];
  plot1 = neuronalerror(10,test_lambda(1),X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('g',';','lambda=',num2str(test_lambda(1)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  plot1 = [];
  plot1 = neuronalerror(10,test_lambda(2),X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('m',';','lambda=',num2str(test_lambda(2)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  plot1 = [];
  plot1 = neuronalerror(10,test_lambda(3),X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('r',';','lambda=',num2str(test_lambda(3)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  plot1 = [];
  plot1 = neuronalerror(10,test_lambda(4),X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('c',';','lambda=',num2str(test_lambda(4)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  plot1 = [];
  plot1 = neuronalerror(10,test_lambda(5),X,Y,Nclases,W1,W2,max_epoch);
  legend=strcat('y',';','lambda=',num2str(test_lambda(5)),';');
  plot(0:rows(plot1)-1,plot1,legend,"linewidth",1);
  printf(legend);
  printf("\n");
  fflush(stdout);

  printf('Se dibujó la gráfica de error \n');
end