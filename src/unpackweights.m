function [W1,W2]=unpackweights(w,fW1,cW1,fW2,cW2)
  
  f=rows(w);
  %Convierte la matriz de pesos en un vector fila
  %recordar que cada vector columna de la matriz relaciona a los pesos de cada entrada
  W1=(reshape(w(1:fW1*cW1,1),cW1,fW1))';
  W2=(reshape(w(fW1*cW1+1:f,1),cW2,fW2))';
  
endfunction