function w=packweights(W1,W2)
  %Convierte la matriz de pesos en un vector fila
  %reshape lee empezando en la fila 1 toda la columna 1 de arriba para abajo
  %por lo que se transpone la matriz de trabajo para que coincida con la lógica
  %de las matrices de pesos

  [fW1,cW1]=size(W1);
	[fW2,cW2]=size(W2);
  
  W1col=(reshape(W1',fW1*cW1,1));
  W2col=(reshape(W2',fW2*cW2,1));
  
  w=[W1col; W2col];
  
  endfunction