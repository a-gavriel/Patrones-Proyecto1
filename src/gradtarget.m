function [gW1,gW2]=gradtarget(W1,W2,X,Y,batchSize=rows(X))

  % usage gradtarget(W1,W2,X,Y,rows(X))
  % 
  % This function evaluates the gradient of the target function on W1 and W2.
  % 
  % W1: weights matrix between input and hidden lay_diffr
  % W2: weights matrix between the hidden and the output lay_diffr
  % X:  training set holding on the rows the input data, unbiased
  % Y:  labels of the training set
  % rows(X): size of the batch

  %% Lay_diffrs:
  global l1a l1b l2a l2b;

  %% Initialize gradients
  gW1 = zeros(rows(W1), columns(W1));
  gW2 = zeros(rows(W2), columns(W2));

  %% Iterates for the batch
  for (i=1:batchSize)
      
      %Forward propagation
      y2b=predict(W1,W2,X(i,:));
      y_diff = y2b-Y(i,:)';

      l2b.backward(y_diff);
      l2a.backward(l2b.gradient);      
      l1b.backward(l2a.gradientX);                
      l1a.backward(l1b.gradient);

      %Accumulates the gradient
      gW1 +=l1a.gradientW;
      gW2 +=l2a.gradientW;
  endfor

  %% Average of gradient from the batch
  gW1/=batchSize;
  gW2/=batchSize;

endfunction;