function j_total=neuronalerror(batchSize, lambda, X, Y, Nclases, W1, W2, max_epoch)  

  global l1a l2a l1b l2b;

  %acciones iniciales
  gW1=zeros(size(W1));
  gW2=zeros(size(W2));

  %Acumulador del error total para graficar
  j_total=[];
  min_error=2.4e-7;

  %Cantidad de lotes a iterar
  num_batches=ceil(rows(X)/batchSize);
  Ndatos=rows(X);
  epoch_counter = 1;

  while(1)  %cantidad de epocas (falta min_error de convergencia)
    %se "desordenan" los datos de entrenamiento al crear un vector de índices
    j_epoch = [];
    
    idx=randperm(rows(X));
    
    %Cálculo por cada mini lotes (Excepto el último)
    for i=1:num_batches-1  %% itera por cada uno de los batches (excepto el último)

      j=idx(batchSize*i-batchSize+1:batchSize*i); %Vector con índices aleatorios

      X_batch=X(j,:); %Obtiene el batch de los índices aleatorios
      Y_batch=Y(j,:); %Valores de Y para el batch elegido

      j_batch=target(W1,W2,X_batch,Y_batch); %Error
      j_epoch = [j_epoch;j_batch]; %Acumulación del error total

      [gW1,gW2]=gradtarget(W1,W2,X_batch,Y_batch); %Gradiente del error

      W1-=lambda*gW1 ;%Actualización de los pesos W1
      W2-=lambda*gW2 ;%Actualización de los pesos W2
      
    endfor

    %cálculo del último minilote
    j=idx(batchSize*(num_batches-1)+1:rows(X)); %Vector con índices aleatorios

    X_batch=X(j,:); %Inicio de batch de X
    Y_batch=Y(j,:); %Valores de Y para el batch

    j_batch=target(W1,W2,X_batch,Y_batch); %Error
    j_epoch = [j_epoch ; j_batch]; %Acumulación del error total

    [gW1,gW2]=gradtarget(W1,W2,X_batch,Y_batch); %Gradiente del error

    W1-=lambda*gW1 ;%Actualización de los pesos W1
    W2-=lambda*gW2 ;%Actualización de los pesos W2

    j_epoch=sum(j_epoch); %Suma el error total de la época
    j_epoch=j_epoch/Ndatos; %Normaliza el error de época
    j_total = [j_total;j_epoch]; %Acumula el error entre épocas

    %Entra si hay más de una época guardada
    if (epoch_counter>1)
      
      dif_epoch = abs(j_epoch-j_total(end-1));

      %Criterio de finalización
      if ((dif_epoch<min_error))||(epoch_counter>max_epoch)
        break;
      endif

    endif

    epoch_counter+=1; %epoch_counter de epocas

  endwhile
end