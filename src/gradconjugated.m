%gradconjugated

% usage function gradconjugated(Ndatos=5000, Nclases=5 , shapeName= 'pie', neuronascapa1=8, 
%              iterations=1000, min_error=2.4e-7)
% 
% Esta funcion realiza un entrenamiento de la red neuronal descrita por los parámetros de entrada
% 
% Inputs:
%   Ndatos: Total de datos a generar para entrenar la red
%   Nclases: Número de clases de salida
%   shapeName: distribution of the samples in the input space:
%          'radial' rings of clases,
%          'pie' angular pieces for each class
%          'vertical' vertical bands
%          'horizontal' horizontal bands
%   neuronascapa1: Número de neuronas en la capa oculta
%   lambda: Tamaño de paso al modificar los pesos
%   total_epoch: Número máximo de épocas a iterar
%   min_error: Error buscado para la convergencia
%   show_error: Boleano para mostrar o no las gráficas del error
%
% Outputs:
%   Figura 1 Datos de entrenamiento
%   Figura 2 Gráfica de error con respecto a la época
%   W1 W2: Matrices con los pesos obtenidos, se guardan en la carpeta 'weigths'
%   Description: Descripción de la configuración y resultado del entrenamiento, se guarda en la carpeta 'weigths'
function gradconjugated(Ndatos=5000, Nclases=3 , shapeName= 'vertical', neuronascapa1=8, 
              iterations=1000, min_error=2.4e-7)

  neuronascapa2=Nclases;

  lddata = input("New training? (0-1): ");
  if (!lddata)
    [X,Y,W1,W2]=loaddata(Nclases,shapeName,neuronascapa1);
    if (!X)
      return;
    endif
  else
    [X,Y]=create_data(Ndatos,Nclases,shapeName);
    hasdistance = input("Use distance? (0-1): ");
    if hasdistance
      X = [X (sum(X.*X,2))];
    endif
    W1=randn(neuronascapa1,columns(X)+1);
    W2=randn(neuronascapa2,neuronascapa1+1);
  endif

  %se grafican los datos creados
  figure(1,'name','Datos de entrenamiento');
  hold off;
  plot_data(X,Y);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % declaración de capas

  global l1a=fullyconnectedbias();
  global l1b=sigmoide();
  global l2a=fullyconnectedbias();
  global l2b=sigmoide();

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  tic;
  [W1_optimized,W2_optimized,iter]=gradconj(W1,W2,X,Y,min_error,iterations); %Gradiente del error
  toc;
  printf("Realizado en %i iteraciones\n", iter);

  y = target(W1_optimized,W2_optimized,X,Y);
  y /= rows(X);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %Guardado de las matrices calculadas a archivos csv
  saveweights("cg_min",X,Y,W1_optimized,W2_optimized,shapeName,Nclases,
      neuronascapa1,Ndatos,rows(X),iter,min_error,y);

endfunction

%Function that computes the gradient of the weights using cg_min
function [W1_optim,W2_optim,iter]=gradconj(W1,W2,X,Y,min_error=2.7e-4,max_iter=0)
  pkg load optim;
  W1_size = size(W1); %Guarda las dimensiones de W1
  W2_size = size(W2); %Guarda las dimensiones de W2
  W_packed = packweights(W1,W2); %Empaqueta los pesos

  %De cg_min se obtiene la matriz gradiente
  %f0: El valor de f en x0
  %El número de iteraciones realizadas
  [W_optim,f0,iter] = cg_min(@targetpacked, @gradpacked, {W_packed, W1_size, W2_size, X, Y}, [2 min_error 0 max_iter]);
  [W1_optim,W2_optim] = unpackweights(-1*W_optim,W1_size(1),W1_size(2),W2_size(1),W2_size(2));

endfunction

%Function that calculates the target from a packed input
function y=targetpacked(cell)
  W1_size = cell{2};
  W2_size = cell{3};
  W_packed = cell{1};
  X = cell{4};
  Y = cell{5};

  [W1,W2] = unpackweights(W_packed,W1_size(1),W1_size(2),W2_size(1),W2_size(2));
  y = target(W1,W2,X,Y);
  y /= rows(X); %Divide entre la cantidad de datos

  clc
  printf ("Current error is %f\n", y);
  fflush(stdout);
endfunction

%Function that calculates the gradtarget from a packed input
function W_optim=gradpacked(cell)
  W1_size = cell{2};
  W2_size = cell{3};
  W_packed = cell{1};
  X = cell{4};
  Y = cell{5};

  [W1,W2] = unpackweights(W_packed,W1_size(1),W1_size(2),W2_size(1),W2_size(2));
  [W1_optim,W2_optim] = gradtarget(W1,W2,X,Y);
  W_optim = packweights(W1_optim,W2_optim);
endfunction