
function y=predict(W1,W2,X)
    
  % usage predict(W1,W2,X)
  % 
  % This function propagates the input X on the neural network to
  % predict the output vector y, given the weight matrices W1 and W2 for 
  % a two-layered artificial neural network.
  % 
  % W1: weights matrix between input and hidden layer
  % W2: weights matrix between the hidden and the output layer
  % X:  Input vector
  global l1a l1b l2a l2b;
  %% Forward prop
  y1a=l1a.forward(W1,X');
  y1b=l1b.forward(y1a);
  y2a=l2a.forward(W2,y1b);
  y=l2b.forward(y2a); %Asigna resultado
  
endfunction;

