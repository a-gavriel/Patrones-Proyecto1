function visualize()
  pkg load statistics;
  
  %shapeLs: 'radial' 'pie' 'vertical' 'horizontal' 
  %clases  
  disp("Choose: 'r'adial, 'p'ie,  'v'ertical, 'h'orizontal") 
  fflush(stdout);
  shapeL = kbhit ();
  disp(shapeL)
  Nclases = input("Choose the amount of clases to clasify: 2-7: ")

  W1name = ["../resources/W1"  shapeL   int2str(Nclases)  ".csv" ];
  W2name = ["../resources/W2"  shapeL   int2str(Nclases)  ".csv" ];

  try
    W1=csvread (W1name);
    W2=csvread (W2name);
  catch
    disp("Matrix files not found!")
    fflush(stdout);
    return;
  end_try_catch

  if(Nclases!=rows(W2))
    disp(["Error! Clases used in files: " int2str(rows(W2))])
    disp(["Error! Clases selected: " int2str(Nclases)])
    return;
  endif


  pix = input("Select image resolution (10-512): ");
  if(pix<10)||(pix>600)
    pix = 512;
  endif

  difumination = input("Use difumination? (0-1): ");

  draw_points = input("Draw training points over the image? (0-1): ");
  change_colors = 0;
  
  if draw_points
    change_colors = input("Draw points with different colors? (0-1): ");
  else
    disp("Warning! The color of some points may show be interchanged")
    fflush(stdout);
  endif

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  global l1a=fullyconnectedbias();
  global l1b=sigmoide();
  global l2a=fullyconnectedbias();
  global l2b=sigmoide();
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %Visualización de resultados
  %imagen 512x512
  colores=['k','r','b','m','g','c','y'];
  Mcolores=[
    0,0,0;
    1,0,0;
    0,0,1;
    1,0,1;
    0,1,0;
    0,1,1;
    1,1,0];


  x=linspace(-1,1,pix);
  [GX1,GX2]= meshgrid(x,x);
  %FX = [GX1(:) GX2(:)];
  % Simulando un clasificador
  %out1=FX(:,2)>-0.1;
  %out2=FX(:,2)<0.1;
  %out3=FX(:,1)>-0.5;
  %out4=FX(:,1)<0.1;
  %GZ = [ out1 out2 out3 out4 ] ./2 ;

  hasdistance = (columns(W1)==4);

  MR=zeros(pix);
  MG=zeros(pix);
  MB=zeros(pix);
  figure(4);
  for pxx=1:pix %% fila
    for pyy=1:pix %%columna      
      PX=GX1(pxx,pyy);
      PY=GX2(pix+1-pxx,pyy);
      pos=[PX PY];
      if hasdistance
        pos=[pos  (PX*PX+PY*PY)];
      endif
      ypred=predict(W1,W2,pos);
      if difumination
        color_r=ypred ./ sum(ypred,1); % convierte en un arreglo de probabilidad (sum(color_r)=1)
      else  
        color_s=ypred  ./ max(ypred);  % se puede utilizar para separar más los colores
        color_r = color_s == 1;          % elimina la difuminación, para utilizarlo se debe 
      endif
      sr=0; %%porcentaje para cada matriz r
      sg=0; %%porcentaje para cada matriz g
      sb=0; %%porcentaje para cada matriz b
      for tt=1:Nclases
        base = color_r(tt,1);
        sr=sr+base*Mcolores(tt,1);
        sg=sg+base*Mcolores(tt,2);
        sb=sb+base*Mcolores(tt,3);
      endfor
      MR(pxx,pyy)=sr;
      MG(pxx,pyy)=sg;
      MB(pxx,pyy)=sb;
    endfor
  endfor
  daspect([1,1,1]);
  mixed = (cat(3,MR,MG,MB));

  
  


  hold on;
  imshow(mixed)
  
  if draw_points
    Xname = ["../sets/X"  shapeL   int2str(Nclases)  ".csv" ];
    Yname = ["../sets/Y"  shapeL   int2str(Nclases)  ".csv" ];

    try
      X=csvread (Xname);
      Y=csvread (Yname);
      plot_data2(X,Y,pix,change_colors);
    catch
      disp("Training files not found! Creating a temporal data set!")
      fflush(stdout);
      if(shapeL=='r')
        shapeName = 'radial';
      elseif (shapeL=='p')
        shapeName='pie';
      elseif (shapeL=='h')
        shapeName='horizontal';
      else
        shapeName='vertical';
      endif
      [X,Y] = create_data(5000,rows(W2),shapeName);
      plot_data2(X,Y,pix,change_colors);
    end_try_catch
    
  endif

  hold off;
endfunction

function plot_data2(X,Y,pix,change_colors)

  % Set all the non-zero entries with the corresponding index
  idx=[1:rows(Y)]'*ones(1,columns(Y));
  idx.*=Y;

  i0=idx(:,1);
  i0(i0==0)=[]; % remove all zeros from the vector
  
  markers = ['+','o','*','x','s','d','^','v','>','<'];
  colors  = ['k','r','b','m','g','c','y'];
  if(change_colors)
    colors  = ['g','c','m','y','k','r','b'];
  endif

  %hold off;
  plot((X(i0,1)+1)*pix/2,(X(i0,2)+1)*pix/2,[colors(1) '+']);
  
  % Markers + o * x s d ^ v > <
  % Colors  k r g b y m c 
  
  
  for i=[2:columns(Y)]
    % Prepare the style recycling the markers and colors
    style=strcat(markers(mod(i-1,length(markers))+1),
     colors(mod(i-1,length(colors))+1));
    % 
    i0=idx(:,i);
    i0(i0==0)=[]; % remove all zeros from the vector
    
    plot((X(i0,1)+1)*pix/2,(X(i0,2)+1)*pix/2,style);
  endfor

  daspect([1,1]);
  grid;
endfunction
