function test()

  %shapes: 'radial' 'pie' 'vertical' 'horizontal' 
  %clases  
  disp("Choose: 'r'adial, 'p'ie,  'v'ertical, 'h'orizontal")
  fflush(stdout);
  shapeL = kbhit ();
  disp(shapeL)
  Nclases = input("Choose the amount of clases to clasify: 2-7: ");

  W1name = ["../resources/W1"  shapeL   int2str(Nclases)  ".csv" ];
  W2name = ["../resources/W2"  shapeL   int2str(Nclases)  ".csv" ];
  
  try
    W1=csvread (W1name);
    W2=csvread (W2name);
  catch
    disp("Matrix training files not found!")
    fflush(stdout);
    return;
  end_try_catch

  if(Nclases!=rows(W2))
    disp(["Error! Clases used in files: " int2str(rows(W2))])
    disp(["Error! Clases selected: " int2str(Nclases)])
    return;
  endif

  loaddata = input("Generate new training XY? (0-1): ");
  if !loaddata
    Xname = ["../sets/X"  shapeL   int2str(Nclases)  ".csv" ];
    Yname = ["../sets/Y"  shapeL   int2str(Nclases)  ".csv" ];
    try
      X=csvread (Xname);
      Y=csvread (Yname);
    catch
      disp("Matrix training files not found!")
      return;
    end_try_catch
  else
    Ndatos = input("Enter amount of samples to generate: ");
    if(shapeL=='r')
        shapeName = 'radial';
      elseif (shapeL=='p')
        shapeName='pie';
      elseif (shapeL=='h')
        shapeName='horizontal';
      else
        shapeName='vertical';
      endif
      [X,Y] = create_data(Ndatos,Nclases,shapeName);
  endif
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  global l1a=fullyconnectedbias();
  global l1b=sigmoide();
  global l2a=fullyconnectedbias();
  global l2b=sigmoide();
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  Xconfusion=X;
  Yconfusion=Y;
  [A,B]=size(Xconfusion);
  comparar=zeros(A,1);
  tipodato=ones(A,1);
  k=Nclases;
  matrizconfusion=zeros(Nclases);
  hasdistance = (columns(W1)==4);

  for g=1:A
    z=Yconfusion(g,:);
    [MM,II]=max(z);
    tipodato(g,1)=II;
  endfor;
  Xconfusion=[tipodato Xconfusion];
  datoscom=Xconfusion(:,1); %guarda la primera filad de los datos %para guardar los nuevos datos
  %pregunta por cada dato dado, para obtener un resultado
  for P=1:A
    class=Xconfusion(P,1);
    classXconfusion=Xconfusion(P,2);
    classYconfusion=Xconfusion(P,3);
    x = [classXconfusion,classYconfusion];
    if hasdistance
      x=[x (classXconfusion*classXconfusion + classYconfusion*classYconfusion)];
    endif 
    salida=predict(W1,W2,x);
    salida=salida';
    [M,I]=max(salida);
    comparar(P,1)=I;
    matrizconfusion(I,class)=matrizconfusion(I,class)+1;
  endfor;
  %se encarga de obtener la cantidad de datos iguales en los calculados Yconfusion dados
  disp("Columns = For this class: ")
  disp("Rows = Predicted the class: ")
  matrizconfusion

endfunction