# Patrones-Proyecto1

- [Patrones-Proyecto1](#patrones-proyecto1)
  - [Consideraciones iniciales:](#consideraciones-iniciales)
  - [Entrenamiento de red neuronal](#entrenamiento-de-red-neuronal)
    - [Por descenso de gradiente](#por-descenso-de-gradiente)
    - [Por gradientes conjugados](#por-gradientes-conjugados)
  - [Resultados](#resultados)
    - [Visualización](#visualizaci%c3%b3n)
    - [Test](#test)

## Consideraciones iniciales:
1.  Abrir una consola en la carpeta principal donde están los archivos descomprimidos.
2.  Ejecutar `octave-cli` en la consola.
3.  Navegar a la carpeta de 'source' con `cd src/`

En este punto puede ejecutar varias acciones.

## Entrenamiento de red neuronal

### Por descenso de gradiente

Para ejecutarlo con los parámetros por defecto, se llama desde la consola a ```graddescent```.

Para llamarlo con parámetros personalizados, se tienen los siguientes parámetros:
```
graddescent(Ndatos, Nclases, shapeName, neuronascapa1, batchSize, lambda, total_epoch, min_error);
``` 
 
 - Inputs:
   - Ndatos: Total de datos a generar para entrenar la red.
   - Nclases: Número de clases de salida.
   - shapeName: distribution of the samples in the input space:
     - 'radial' rings of clases
     - 'pie' angular pieces for each class
     - 'vertical' vertical bands
     - 'horizontal' horizontal bands
   - neuronascapa1: Número de neuronas en la capa oculta.
   - batchSize: Tamaño de cada minilote a evaluar.
   - lambda: Tamaño de paso al modificar los pesos.
   - total_epoch: Número máximo de épocas a iterar.
   - min_error: Error buscado para la convergencia.
   - show_error: Boleano para mostrar o no las gráficas del error.

 - Outputs:
   - Figura 1 Datos de entrenamiento.
   - Figura 2 Gráfica de error con respecto a la época.
   - W1 W2: Matrices con los pesos obtenidos, se guardan en la carpeta 'resources'.
   - Description: Descripción de la configuración y resultado del entrenamiento, se guarda en la carpeta 'resources'.
   - (Opcional): Gráficas de error, se crean 2 figuras adicionales con redes neuronales casi con los mismos parámetros.
     - La primera gráfica se compone de redes neuronales con diferentes batchSize.
     - La segunda gráfica se compone de redes neuronales con diferentes lambdas.

Cuando se corre el comando, se puede seleccionar si se crea una nueva red neuronal o se quiere entrenar una red neuronal a partir de pesos guardados anteriormente.

> New training? (0-1):

Con 0 para re-entrenar y 1 para un nuevo entrenamiento. Para el primer caso, se buscan archivos de pesos según las configuraciones de entrada; en caso de no existir archivos compatibles, se retorna error.

### Por gradientes conjugados

Para ejecutarlo con los parámetros por defecto, se llama desde la consola a ```gradconjugated```.

Para llamarlo con parámetros personalizados, se tienen los siguientes parámetros:
```
gradconjugated(Ndatos, Nclases, shapeName, neuronascapa1, iterations, min_error);
``` 
 - Inputs:
   - Ndatos: Total de datos a generar para entrenar la red.
   - Nclases: Número de clases de salida.
   - shapeName: distribution of the samples in the input space:
     - 'radial' rings of clases
     - 'pie' angular pieces for each class
     - 'vertical' vertical bands
     - 'horizontal' horizontal bands
   - neuronascapa1: Número de neuronas en la capa oculta.
   - min_error: Error buscado para la convergencia.

 - Outputs:
   - Figura 1 Datos de entrenamiento.
   - Figura 2 Gráfica de error con respecto a la época.
   - W1 W2: Matrices con los pesos obtenidos, se guardan en la carpeta 'resources'.
   - Description: Descripción de la configuración y resultado del entrenamiento, se guarda en la carpeta 'resources'.

Cuando se corre el comando, se puede seleccionar si se crea una nueva red neuronal o se quiere entrenar una red neuronal a partir de pesos guardados anteriormente.

> New training? (0-1):

Con 0 para re-entrenar y 1 para un nuevo entrenamiento. Para el primer caso, se buscan archivos de pesos según las configuraciones de entrada; en caso de no existir archivos compatibles, se retorna error.

## Resultados

### Visualización
Para visualizar una imagen con un nuevo set de datos:
```
visualize()
```
Seleccionar el tipo de distribución:
> Choose: 'r'adial, 'p'ie,  'v'ertical, 'h'orizontal

Seleccionar la cantidad de clases a clasificar:
> Choose the amount of clases to clasify: 2-7:

Seleccionar un tamaño de resolución X y Y de la imagen a generar entre el rango de 10-512:
> Select image resolution (10-512):

Elegir si se desea difuminación:
> Use difumination? (0-1):

Elegir si se desea visualizar los puntos de entrenamiento sobre la imagen:
> Draw training points over the image? (0-1):

Si se plaea visualizar los puntos se preguntará si se desea utilizar los mismos colores (se recomienda que sean distintos): 
> Draw points with different colors? (0-1):

Si no se encuentra el conjunto de entrenamiento utilizado se intentará crear un nuevo conjunto con 5000 datos.


### Test
Para visualizar unaa matriz de confusión para probar la red:
```
test()
```
Seleccionar el tipo de distribución:
> Choose: 'r'adial, 'p'ie,  'v'ertical, 'h'orizontal

Seleccionar la cantidad de clases a clasificar:
> Choose the amount of clases to clasify: 2-7:

Elegir si se desea utilizar un nuevo set de entrenamiento para realizar la prueba (Se recomienda que sí):
> Generate new training XY? (0-1):

Ingresar la cantidad de muestras a generar
> Enter amount of samples to generate:
